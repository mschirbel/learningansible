# Services

Agora que temos tudo instalado em nossas máquinas, precisamos dizer ao Ansible como iniciá-las ou reiniciá-las.

Vamos ver o exemplo de nosso load balancer:

```yaml
---
- hosts: loadbalancer
  become: true
  tasks:
    - name: install nginx
      apt: name=nginx state=present update_cache=yes

```

Vamos criar uma nova task:

```yaml
---
- hosts: loadbalancer
  become: true
  tasks:
    - name: install nginx
      apt: name=nginx state=present update_cache=yes
    - name: ensure nginx started
      service: name=nginx state=started enabled=yes

```

Quando chegar na nova task, o Ansible vai verificar se o estado do serviço é running, e caso não seja, irá iniciá-lo.

Outra opção para state é restart, na qual o Ansible irá reiniciar o serviço, qualquer que seja o status dele.

Agora executamos nosso playbook:

```sh
ansible-playbook loadbalancer.yml
```

Agora vamos ver o retorno de nosso serviço:

```bash
wget -q0- http://[ip] | less
```

Deverá aparecer a página default do nginx.

## Instalando o curl na máquina de controle

Crie um novo arquivo de playbook para a máquina de controle:

```
touch control.yml
```

E para o arquivo control.yml:

```yaml
---
- hosts: control
  become: true
  tasks: 
    - name: install tools
      apt: name={{items} state=present update_cache=yes
      with_items:
        - curl
```

Agora executamos nosso playbook:

```sh
ansible-playbook control.yml
```

## Atualizando o webserver e o database

Agora que vimos como garantir que um serviço vai estar sempre running, vamos fazer isso no webserver e no databse:

webserver.yml:

```yaml
---
-  hosts: webserver
   become: true
   tasks:
     -  name: install web components
        apt: name={{item}} state=present update_cache=yes
        with_items:
          - apache2
          - libapache2-mod-wsgi
          - python-pip
          - python-virtualenv
     - name: ensure apache2 started
       service: name=apache2 state=started enabled=yes
```

database.yml:

```yaml
---
- hosts: database
  become: true
  tasks:
    - name: install mysql-server
      apt: name=mysql-server state=present update_cache=yes
    - name: ensure mysql started
      service: name=mysql state=started enabled=yes
```

Agora executamos nossos playbooks:

```sh
ansible-playbook webserver.yml
ansible-playbook db.yml
```

---

Temos que verificar se os módulos que instalamos do Apache estão funcionando. Para isso, podemos usar um módulo do Ansible para o Apache que realizada essa função.

```yaml
---
-  hosts: webserver
   become: true
   tasks:
     -  name: install web components
        apt: name={{item}} state=present update_cache=yes
        with_items:
          - apache2
          - libapache2-mod-wsgi
          - python-pip
          - python-virtualenv
     -  name: ensure apache2 started
        service: name=apache2 state=started enabled=yes
     -  name: ensure mod_wsgi enabled
        apache2_module: state=present name=wsgi
```

Sempre que queremos um novo módulo no Apache, precisaríamos de um restart. Mas se fôssemos incluir uma nova task para isso teríamos um problema, pois toda vez que esse playbook fosse usado, nosso apache seria iniciado e depois sofreria um restart.

Para resolver isso, temos os Services Handlers. São um tipo especial de task.

```yaml
---
-  hosts: webserver
   become: true
   tasks:
     -  name: install web components
        apt: name={{item}} state=present update_cache=yes
        with_items:
          - apache2
          - libapache2-mod-wsgi
          - python-pip
          - python-virtualenv
     -  name: ensure apache2 started
        service: name=apache2 state=started enabled=yes
     -  name: ensure mod_wsgi enabled
        apache2_module: state=present name=wsgi
        notify: restart apache2
   handlers:
     -  name: restart apache2
        service: name=apache2 state=restarted
```

Para o handler, ele só irá funcionar baseado num trigger, esse sendo a tag notify em nossa task de módulo.

O notify só é ativado quando há uma change naquela task. Ou seja, se o módulo for instalado. Caso contrário, ele não ativa e assim não ativa nosso handler.

Agora execute o playbook.

```sh
ansible-playbook webserver.yml
```

Veja se realmente funcionou:

```sh
curl [ip do app01]
```