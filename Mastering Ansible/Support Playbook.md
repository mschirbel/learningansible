# Support Playbook

Em adição a tudo que foi feito, temos a necessidade de manter a nossa infraestrutura em quanto ela se desenvolve.

Por exemplo, caso seja preciso parar toda nossa stack, ou reiniciá-la de uma vez.

```yaml
---
# Bring stack down
- hosts: loadbalancer
  become: true
  tasks:
    - service: name=nginx state=stopped

- hosts: webserver
  become: true
  tasks:
    - service: name=apache2 state=stopped

# Restart mysql
- hosts: database
  become: true
  tasks:
    - service: name=mysql state=restarted

# Bring stack up
- hosts: webserver
  become: true
  tasks:
    - service: name=apache2 state=started

- hosts: loadbalancer
  become: true
  tasks:
    - service: name=nginx state=started
```

Ao parar nossa stack, não queremos parar o database primeiro, depois o LB e depois o DB. E ao inciá-la também temos uma ordem específica.

No playbook estamos usando restart no mysql porque ele é o último a ser parado e o primeiro a ser iniciado.

## Status: wait_for

Agora que nossa stack está no lugar, estamos usando comandos 'curl' para identificar e realizar algum certo troubleshooting.

Mas isso não é nada automatizado. Podemos usar o Ansible para isso.

Vamos criar um novo playbook:

```bash
cd playbooks
touch stack_status.yml
```

Dentro desse arquivo:

```yaml
---
- hosts: loadbalancer
  become: true
  tasks:
    - name: verify nginx status
      command: service nginx status

- hosts: webserver
  become: true
  tasks:
    - name: verify apache status
      command: service apache status

- hosts: db
  become: true
  tasks:
    - name: verify mysql status
      command: service mysql status
```

Agora execute o playbook.

Caso o comando retorne que o serviço não está ativo, conseguiríamos ver isso no result do playbook.

E usamos command, pois queremos algo que seja read-only.

---

## Wait_for

Também queremos saber quais portas estamos escutando em cada porta. Para isso, usaremos o módulo wait_for.

```yaml
---
- hosts: loadbalancer
  become: true
  tasks:
    - name: verify nginx status
      command: service nginx status

    - name: verify nginx is listening on port 80
      wait_for: port=80 timeout=5

- hosts: webserver
  become: true
  tasks:
    - name: verify apache status
      command: service apache status
    
    - name: verify apache is listening on port 80
      wait_for: port=80 timeout=5

- hosts: db
  become: true
  tasks:
    - name: verify mysql status
      command: service mysql status
    
    - name: verify mysql is listening on port 3306
      wait_for: port=3306 timeout=5
```

Agora execute o playbook.

---

Agora no nosso playbook para fazer um restart da stack será alterado com o comando wait_for, para que um serviço não seja iniciado antes que o outro, ou os necessários, também estejam online.

```yaml
---
# Bring stack down
- hosts: loadbalancer
  become: true
  tasks:
    - service: name=nginx state=stopped
    - wait_for: port=80 state=drained

- hosts: webserver
  become: true
  tasks:
    - service: name=apache2 state=stopped
    - wait_for: port=80 state=stopped

# Restart mysql
- hosts: database
  become: true
  tasks:
    - service: name=mysql state=restarted
    - wait_for: port=3306 state=started

# Bring stack up
- hosts: webserver
  become: true
  tasks:
    - service: name=apache2 state=started
    - wait_for: port=80 state=started

- hosts: loadbalancer
  become: true
  tasks:
    - service: name=nginx state=started
    - wait_for: port=80
```

State drained é para verificar se o serviço está UP e se as conexões com aquela porta foram totalmente encerradas.

---

## Stack Status: URI, register, fail, when

Até o momento, só testamos se os serviços estão ativos e quais as portas eles ouviram. Mas agora, precisamos saber se o conteúdo que é retornado está correto.

Vamos começar editando o loadbalancer:

```yaml
---
- hosts: loadbalancer
  become: true
  tasks:

    - name: install tools
      apt: name={{items}} state=present update_cache=yes
      with_items:
       - python-httplib2

    - name: install nginx
      apt: name=nginx state=present update_cache=yes

    - name: ensure nginx started
      service: name=nginx state=started enabled=yes

    - name: configure nginx site
      template: src=templates/nginx.conf.j2 dest=/etc/nginx/sites-avaliable/demo mode=0644
      notify: restart ngix

    - name: de-activate default nginx site
      file: path=/etc/nginx/sites-enable/default state=absent
      notify: restart nginx

    - name: activate demo nginx site
      file: src=/etc/nginx/sites-avaliable dest=/etc/nginx/sites-enabled/demo state=link
      notify: restart nginx

   handlers:
     - name: restart nginx
       service: name=nginx state=restarted
```

Agora executamos esse playbook.

Instalamos essa lib no loadbalancer, pois faremos essa verificação do loadbalancer para o servidores de aplicação - testando nossa URI.

E precisamo disso no control host também para testarmos o loadbalancer.

```yaml
- hosts: control
  become: true
  tasks:
    - name: install tolls
      apt: name={{item}} state=present update_cache=yes
      with_items:
       - curl
       - python-httplib2
``` 

Execute esse playbook.

---

--- Agora voltando para nosso stack status playbook:

```yaml
---
- hosts: loadbalancer
  become: true
  tasks:
    - name: verify nginx status
      command: service nginx status

    - name: verify nginx is listening on port 80
      wait_for: port=80 timeout=5

- hosts: webserver
  become: true
  tasks:
    - name: verify apache status
      command: service apache status
    
    - name: verify apache is listening on port 80
      wait_for: port=80 timeout=5

- hosts: db
  become: true
  tasks:
    - name: verify mysql status
      command: service mysql status
    
    - name: verify mysql is listening on port 3306
      wait_for: port=3306 timeout=5

- hosts: control
  tasks:
    - name: verify end-to-end response
      uri: url=http://{{item}} return_content=yes
      with_item: groups.loadbalancer
      register: lb_index
    
    - fail: msg="index failed to return content"
      when: "'Hello, from sunny' not it item.content"
      with_items: "{{lb_index.results}}

    - name: verify end-to-end db response
      uri: url=http://{{item}}/db return_content=yes
      with_item: groups.loadbalancer
      register: lb_db
    
    - fail: msg="db failed to return content"
      when: "'Database Connected from' not it item.content"
      with_items: "{{lb_db.results}}

- hosts: loadbalancer
  tasks:
    - name: verify backend response
      uri: url=http://{{item}} return_content=yes
      with_item: groups.webserver
      register: app_index
    
    - fail: msg="index failed to return content"
      when: "'Hello, from sunny {{item.item}}!' not it item.content"
      with_items: "{{app_index.results}}

    - name: verify backend db response
      uri: url=http://{{item}}/db return_content=yes
      with_item: groups.webserver
      register: app_db
    
    - fail: msg="index failed to return content"
      when: "'Database Connected from {{item.item}}!' not it item.content"
      with_items: "{{app_db.results}}
```

return_content é o responsável por falar que não é somente a pagína default do framework, mas que é a página de nossa aplicação.

usamos um with_items, pois caso um dia seja necessário acrescentar mais um loadbalancer, já está correto.

E agora vamos adicionar um register para salvar o conteúdo para ser avaliado posteriormente.

A task de fail é como vamos testar se o conteúdo retornou erro. Ou seja, falhou quando...(e isso vem no parâmetro when).

Isso facilita, pois não precisamos lembrar quais os endpoints específicos.

Agora basta executar o playbook.