# External Roles & Galaxy

Galaxy é um repositório online para roles.

Pode ser acessado ![aqui](https://galaxy.ansible.com/)

Se você tem algo que seja comum, é possível que alguem ja tenha feito. É bom para pegar ideias também.

Para instalar uma role

```
ansible-galaxy install [role]


Veja se a role tem um bom rating e se é usada por um tempo bom.