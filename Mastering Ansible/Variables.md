# Variables

Agora queremos substituir as variáveis "hardcoded" para que nossa app fique mais segura.

## Fact

Vamos começar com o MySQL:

Dentro de nosso arquivo roles/mysql/tasks/main.yml temos um valor de bind-address que está explícito para 0.0.0.0

Isso expões a qualquer IP que tentar conectar pela porta 3306. Mas isso pode ser prejudicial, queremos que ele escute somente os IPs das máquinas de aplicação.

Para isso, podemos usar uma variável dinâmica que o Ansible nos provém que é o *fact*

Para saber os facts que o Ansible pode coletar, podemos usar um módulo chamado setup:

```sh
ansible -m setup db01
```

E podemos substituir isso em nosso arquivo roles/mysql/tasks/main.yml:

```yaml
- name: ensure mysql listening on all ports
  lineinfile: dest=/etc/mysql/my.cnf regexp=^bind-address line="bind-address = {{ ansible_eth0.ipv4.address }}"
  notify: restart mysql
```

Agora que quando o Ansible funcionar, ele vai procurar dinamicamente pelo IP e assim vai se conectar. E podemos executar o playbook.

```bash
ansible-playbook db.yml
ansible-playbook status_stack.yml
```

Veja que no playbook de status, houve uma falha. Pois por default, ele somente está olhando o 127.0.0.1:3306. Mas com a variável dinâmica, nós trocamos esse IP.

Dentro do playbook de stack_status:

```yaml

- hosts: db
  become: true
  tasks:
    - name: verify mysql status
      command: service mysql status
    
    - name: verify mysql is listening on port 3306
      wait_for: host={{ ansible_eht0.ipv4.address }}port=3306 timeout=5
```

Adicione o parâmetro host para o modulo wait_for. Assim, vamos sempre checar de um host específico.

E dentro do playbook de stack_restart:

```yaml
# Restart mysql
- hosts: database
  become: true
  tasks:
    - service: name=mysql state=restarted
    - wait_for: host={{ ansible_eht0.ipv4.address }} port=3306 state=started
```

Tudo parece estar funcionando perfeitamente.

---

## Defaults

Dentro de nosso arquivo roles/mysql/tasks/main.yml vemos que explicitamos o user/password de nosso banco de dados, e até mesmo o host.

Sabemos que esses dados sempre vão existir, apenas precisamos fazer que isso seja mais genérico.

No arquivo roles/mysql/tasks/main.yml:

```yaml
  - name: create database
    mysql_db: name={{ db_name }} state=present

  - name: create user
    mysql_user: name={{ db_user_name }} password={{ db_user_pass }} priv={{ db_name }}.*:ALL host='{{ db_user_host }}' state=present
```

Agora estamos esperando quatro variáveis diferentes. Para inserir essas variáveis, podemos fazer de uma forma que, mesmo a pessoa não tenha as variáveis ela consiga subir o serviço com dados genéricos

Para isso, vamos usar o defaults file, dentro de roles/mysql/defaults/main.yml:

```yaml
---
db_name: myapp
db_user_name: dbuser
db_user_pass: dbpass
db_user_host: localhost
```

Mas ainda assim, podemos fazer um override nos defaults.

---

## Vars

As variáveis defaults são as que tem menos prioridades em qualquer Ansible run. Se nada der certo, use isso.

As que tem mais prioridade são as extra vars, pois podem ser usadas em cli com *-e*. Mas isso não é nada seguro.

Entre essas duas opções, existem diversas que podemos ver na documentação oficial.

Dentro da pasta roles/mysql/ veja que existe um diretório vars/ com um arquivo main.yml Esses são os que mais prioridade.

Podemos usar vars dentro de uma task, com o módulo vars. Mas para manter um padrão, vamos deixar tudo do mesmo jeito, e do jeito mais simples possível.

E toda variável é global, ou seja, sempre vai estar num namespace global. Nesse exemplo vamos usar de três jeitos:

1. defaults
2. dentro de uma role
3. usando group vars - são usados automaticamente

*Keep you variables simple*

Dentro de nosso arquivo templates/db.yml:

 ```yaml
---
- hosts: database
  become: true
  roles:
    - { role: mysql, db_name: demo, db_user_name: demo, db_user_pass: demo, db_user_host: '% }'
```

---

## with_dict

with_dict é uma variação para o with_items, como é um mapeador de kay-value, para ele podem ser atribuídos diversos indices.

Para nosso arquivo roles/nginx/defaults/main.yml:

```yaml
---
sites:
  myapp:
    frontend: 80
    backend: 80
```

Agora precisamos atualizar nossos templates para usarem essas configurações.

roles/nginx/tasks/main.yml:

```yaml
---
- name: install tools
  apt: name={{items}} state=present update_cache=yes
  with_items:
    - python-httplib2

- name: install nginx
  apt: name=nginx state=present update_cache=yes

- name: configure nginx site
  template: src=nginx.conf.j2 dest=/etc/nginx/sites-avaliable/{{ item.key }} mode=0644
  with_dict: sites
  notify: restart ngix

- name: de-activate default nginx site
  file: path=/etc/nginx/sites-enable/default state=absent
  notify: restart nginx

- name: activate nginx site
  file: src=/etc/nginx/sites-avaliable/{{ item.key }} dest=/etc/nginx/sites-enabled/{{ item.key }} state=link
  with_dict: sites
  notify: restart nginx

- name: ensure nginx started
  service: name=nginx state=started enabled=yes
```

Agora podemos usar o item.key para referenciar o dicionário.

E para nosso arquivo nginx.conf.j2:

```j2
upstram {{ item.key }} {
{% for server in groups.webserver %}
   server {{ server }}:{{ item.value.backend }};   
{% endfor}
}

server {
   listen {{ item.value.frontend }};

   location /{
      proxy_pass http://{{ item.key }};
   }
}
```

Pode parecer meio estranho usar a sintaxe do ansible dentro de um j2, mas isso é possível pois o objeto do dict vai estar diretamente na task, assim ele pode ser referenciado.

Agora basta executar o playbook:

```
ansible-playbook loadbalancer.yml
```

---

## Como manter somente um site ativo no loadbalancer

Se logarmos em nosso loadbalancer e olharmos os sites habilitados:

```sh
ssh lb01
ls -la /etc/nginx/sites-enabled/
```

Veja que ainda temos o nosso site demo. Isso aconteceu, pois quando o demo saiu do playbook, o Ansible não sabia mais de sua existência. Mas ainda está no sistema.

Precisamos deixar explícito que queremos criar um **único** site.

Dentro de nosso arquivo roles/nginx/tasks/main.yml:

```yaml
- name: get active sites
  shell: ls -1 /etc/nginx/sites-enabled
  register: active

- name: de-activate sites
  file: path=/etc/nginx/sites-enable/{{ item }} state=absent
  with_items: active.stdout_lines
  when: item not in sites
  notify: restart nginx
```

Adicionamos e mudamos uma task.

A primeira, vai pegar os sites que estão habilitados e salvar em uma variável - active.

A segunda vai ver quais sites não estão em nossa configuração - nginx.conf.j2 - e vai apagá-lo.

Agora vamos executar:

```
ansible-playbook loadbalancer.yml
```

---

## Vars Continued

O principal do demoapp é manter a encapsulação que usamos para nossas libs.

E queremos manter uma conectividade fácil.

roles/demo_app/tasks/main.yml

```yaml

- name: copy demo.wsgi
  template: src=demo.wsgi.j2 dest=/var/www/demo.wsgi mode=0755
  notify: restart apache2
```

Um problema que ocorre é:
Quando alguem fizer alguma referência, ela nao precisa ser absoluta, ou seja, nossa aplicação tem que reconhecer a mesma coisa sendo chamada de formas diferentes:

templates/webserver.yml

```yaml
---
-  hosts: webserver
   become: true
   roles:
     - apache2
     - { role: demo_app, db_name: demo, db_user: demo, db_pass: demo}
```

---

## Vars_file e group_vars

No momento, temos o user e senha do banco de dados guardados em dois lugares. Idealmente, gostaríamos de ter em somente um lugar.

Teremos um arquivo somente para guardar as variáveis.

Ansible nos permite fazer isso de três formas:

1. Inventory
2. Vars_file
3. Group_vars

Noss inventory é para guardar somente os hosts, nao queremos mistturar as coisas
Vars_file é bom, mas é necessário um include sempre que for usar
Group_Vars é o que vamos usar, pois quando colocado no lugar correto, nao precisamos referenciar.

Primeiramente crie um diretório:

```bash
mkdir group_vars
touch all.yml
```

por causa do nome: group_vars/all
todos os grupos vao se beneficiar desse arquivo, sendo assim, muito mais facil de usar em varios lugares.

para o arquivo all.yml:

```yaml
---
db_name: demo
db_user: demo
db_pass: demo
```

Agora, em nosso templates/db.yml:

```yaml
---
- hosts: database
  become: true
  roles:
    - role: mysql
      db_user_name: "{{ db_user }}"
      db_user_pass: "{{ db_pass }}"
      db_user_host: '%'
```

Agora, para nosso arquivo templates/webserver.yml:

```yaml
---
-  hosts: webserver
   become: true
   roles:
     - apache2
     - demo_app
```

Agora execute o playbook do database e do webserver.

---

## Vault

Como o propósito do Ansible é a colaboração, vamos querer subir essa infra para um repositório publico. Acontece que, guardar as senhas em texto assim, é algo complicado, pois é uma falha de segurança enorme.

Ansible tem uma ferramenta para tratar isso, o Ansible vault. Vault criptografa um arquivo com uma passphrase, mas é o arquivo todo. Quando executar o Ansible, basta falar qual a passphrase e tudo estará no lugar.

Mas isso é um pouco chato, porque o Ansible criptografa o arquivo todo. Mas o Ansible fornece um workaround que é parear um arquivo vars_file com um criptografado.

```bash
cd group_vars
mv all vars
mkdir all
mv vars all
export EDITOR=vi
cd all
ansible-vault create vault
```

Ao invés de ter um arquivo all, temos uma pasta. E tudo que estiver ali será visível para todos.

Ao criar o vault, será necessário uma senha - passphrase

E agora, teremos que editar o arquivo, perceba que ele está em /tmp/[NOME GENÉRICO] isso se deve porque ele será editado e antes de ser salvo ao filesystem, será criptografado.

Dentro do arquivo:

```yaml
vault_db_pass: DNInoianiADNodnAUD
```

Salve e saia e veja que existe agora um arquivo vault.

Para editar:

```sh
ansible-vault edit vault
```

E temos que digitar a senha. E para o arquivo group_vars/all/vars:

```yaml
---
db_name: demo
db_user: demo
db_pass: "{{ vault_db_pass }}"
```

Ao tentar executar

```sh
ansible-playbook site.yml
```

Um erro ocorrerá informado que não foi possível porque a gente tem uma senha. Para isso:

```sh
ansible-playbook --ask-vault-pass [senha] site.yml
```
ou

Podemos criar um arquivo para guardar a passphrase

```sh
echo "masteransiblepass" > templates/.vault_pass.txt
chmod 0600 !$
```

Para que o Ansible encontre esse arquivo colocamos em nosso custom ansible.cfg:

```cfg
[defaults]
inventory =./dev
vault_password_file = templates/.vault_pass.txt
```

E agora podemos executar nossos playbooks.