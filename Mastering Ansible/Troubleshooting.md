# Troubleshooting

## Ordering Problem

Veja o exemplo abaixo:

```yaml
- name: ensure mysql started
  service: name=mysql state=started enabled=yes
  tags: [ 'service' ]

- name: ensure mysql listening on all ports
  lineinfile: dest=/etc/mysql/my.cnf regexp=^bind-address line="bind-address = {{ ansible_eth0.ipv4.address }}"
  notify: restart mysql
  tags: [ 'configure' ]
```

Suponha que ao executar pela primeira vez, alguma configuração tenha sido feita de forma errada na segunda task. Ao corrigí-la e executar o playbook novamente, a primeira task deve assegurar que o MySQL esteja funcionando, mas se por alguma razão, decorrente da primeira execução, ele estiver pausado, nunca chegaremos na segunda task.

Para isso, devemos reordenar nosso playbook, uma delas é usar o *ignore_errors*:

```yaml
- name: ensure mysql started
  service: name=mysql state=started enabled=yes
  ignore_erros: true
  tags: [ 'service' ]

- name: ensure mysql listening on all ports
  lineinfile: dest=/etc/mysql/my.cnf regexp=^bind-address line="bind-address = {{ ansible_eth0.ipv4.address }}"
  notify: restart mysql
  tags: [ 'configure' ]
```

Apesar de aparecer um erro, ele iria pular para a próxima task.

Mas o melhor jeito é pensar logicamente no que está acontecendo e trocar a ordem das tasks:

```yaml
- name: ensure mysql listening on all ports
  lineinfile: dest=/etc/mysql/my.cnf regexp=^bind-address line="bind-address = {{ ansible_eth0.ipv4.address }}"
  notify: restart mysql
  tags: [ 'configure' ]

- name: ensure mysql started
  service: name=mysql state=started enabled=yes
  tags: [ 'service' ]
```

O melhor jeito é usar o Ansible e evitar correções manuais.

---

## Jumping tasks

Ao invés de ficar comentando o playbook para pular tasks, podemos usar um comando:

```bash
ansible-playbook site.yml --step
```

y: aceitar a task
n: rejeitar a task
c: continuar sem perguntar, até o final.
ctrl+c: cancelar o playbook

Ansible nos perguntará se queremos ou não executar cada task. Isso é um bom jeito de segmentar o playbook e manter a confiança no que deve ser executado.

O outro jeito é iniciar em algum lugar específico:

```bash
ansible-playbook site.yml --list-tasks
```

Com isso vemos o nome de cada task:

```bash
ansible-playbook site.yml --start-at-task "nome da task"
```

---

## Retrying Failed Hosts

Como saber quais servidores devem ser tratados quando falham?

```bash
ansible-playbook site.yml
```

Ao falhar um host, o Ansible cria em seu home um arquivo temporário com os hosts:

```
less /home/ansible/site.retry
```

Caso o problema seja corrigido, na próxima execução podemos fazer:

```bash
ansible-playbook site.yml --limit @/home/ansible/site.retry
```

---

## Syntax-Check

Queremos ter um pouco mais de segurança ao aplicar as changes que estamos fazendo em nossos playbooks.

Uma coisa bem comum é ter um ambiente de dev ou homol.

Mas existem outros comandos para nos auxiliar com essa tarefa.
Como por exemplo:

```bash
ansible-playbook --syntax-check site.yml
```

Isso faz um parse no playbook e mostra o resultado.

Outro jeito de se fazer é Drive Run
O Ansible analisa o que vai fazer, mas não faz de verdade. Só te mostra o plano de ação:

```bash
ansible-playbook --check site.yml
```

Nem todos os módulos suportam o check, mas já ajuda bastante.

---

## Debug

É uma mensagem que pode ser usada para identificar onde na execução do seu playbook houve uma falha:

```yaml
- debug: var=active.stdout_lines
```

Ao executar o playbook, veremos a saída dessa variável no meio da execução.