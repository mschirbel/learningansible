# Introduction

O curso será voltado para orquestração e criação de workflows.

Como usar Ansible com Infra as a Code.

## Passos

1. Playbooks and Modules
2. Refactoring with Roles
3. Speed and Optimization

E por último vamos falar sobre troubleshooting.

## Configuration Management

O propósito é reduzir o tempo de changes, usando códigos ou scripts para realizar o processo manual e humano.

Isso melhora a qualidade e reduz o tempo de execução.

Isso também é bom, pois permite que outras pessoas possam ver as alterações e permite que o código seja executado em outros ambientes.

Traz também o controle de versão, o que controla mais o ambiente.

As ferramentas de Configuration Management trazem abstrações que agilizam os delpoys ou as mudanças necessárias no ambiente, bem como frameworks independentes e organizados.

Ansible permite a criação de workflows determinados, por exemplo, permite realizar tasks em determinados servidores antes de outros.