# Inventory

Podemos ver dois links oficiais sobre esse assunto:

![aqui](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html) e ![aqui](https://docs.ansible.com/ansible/latest/user_guide/intro_dynamic_inventory.html)

Agora podemos começar a executar nossos primeiros comandos.

Os comandos de Ansible tem o comando que queremos executar e os alvos.

Devemos especificar todos os alvos de nossos comandos, e para fazermos isso, devemos popular um inventário.

Pode ser um arquivo estático ou dinâmico. Vamos focar em um estático para começar.

Um inventário pode armazenar os hostnames e outras informações, sobre como conetar aos hosts, como custom ports, por exemplo.

Claro que podemos fazer isso via cli, mas com isso no Inventário, fica mais fácil de administrar.

Também permite agrupar em grupos nossos hosts, executando comandos nos grupos.

Podemos ver nossas listas de hosts no inventário, digitando:

```
ansible --list-hosts all
```

Para visualizar esse arquivo:

```
sudo vi /etc/ansible/hosts
```

Podemos adicionar nossos servidores nesse arquivo, mas isso levanta um problema. Pois teríamos todos os hosts dentro da nossa máquina de controle. E todas as vezes que fossemos mudar os hosts, deveríamos mudar dentro da nossa máquina de controle.

Uma boa prática, seria deixar isso em um arquivo separado, e localmente. E popular esse arquivo, deixando-o próximo aos playbooks e as roles, dentro de um repositório do Git.

## Creating a Inventory

Vamos criar uma pasta que vai conter nossos inventários

```
mkdir ansible_inventory
```

Para criar um inventário, devemos escrever no formato YAML.

Nesse exemplo, vamos criar grupos e inserir os hosts

```
cd ansible_inventory
vim dev
```

E para inserir os hosts ali dentro, deve-se seguir o formato:

```
[loadbalancer]
lb01

[webserver]
app01
app02

[database]
db01

[control]
control
```

Não devemos esquecer de inserir a máquina de controle, pois queremos que todas as atualizações sejam feitas usando o Ansible.

Claro que é necessário instalar manualmente o Ansible na máquina de controle, mas após isso podemos automatizar todas as atualizações.

Para visualizar os hosts que foram adicionados no arquivo, temos que usar o parâmetro -i:

```
ansible -i dev --list-hosts all
```

E assim podemos ver todos os hosts que inserimos.

Por default, o Ansible tenta fazer SSH com todos os hosts que estão nos arquivos. Mas como colocamos a própria máquina de controle no arquivo, ele vai tentar conectar consigo mesmo. Isso é desnecessário, para explicitar isso no código, podemos passar parâmetros indicando que aquele host é local:

```
[control]
control ansible_connnection=local
```

---

Para tornar as coisas ainda mais fáceis, queremos retirar esse parâmetro -i, pois o Ansible olha primeiramente no /etc/ansible/hosts.

Para isso, devemos trocar a linha do arquivo de configuração do Ansible, que fica em /etc/ansible/ansible.cfg

```
vi /etc/ansible/ansible.cfg
```

Procure pela linha:

```
inventory   =   /etc/ansible/hosts
```

E troque pelo path do hosts local que foi criado.

Ou, podemos fazer algo ainda melhor que é:

Criar um arquivo de configuração do Ansible:

```
touch ansible.cfg
```

E nele escrever:

```
[defaults]
inventory = ./dev
```

E agora se rodarmos

```
ansible --list-hosts all
```

Vemos que temos nosso arquivos host criado e sem passar o parâmetro -i.