# Playbooks

São basicamente a parte mais importante de todo o Ansible.

Devemos prestar atenção em 4 fatores que podem determinar o sucesso de um playbook:

## 4 Factors

1. Packages - sejam de um package manager, de algum repositório ou até mesmo localmente.
2. Service Handler - monitorar o serviço, algum supervisor
3. A configuração do sistema, sejam arquivos, diretórios, permissões, usuários, firewall
4. Arquivos de configuração da aplicação

## Workflow

Vamos escolher um módulo, verificar quais são os requisitos, instalá-los e fazer o módulo funcionar.

Podemos ver uma lista completa dos módulos ![aqui](https://docs.ansible.com/ansible/2.5/modules/modules_by_category.html)

Quando estiver procurando qual módulo usar, devemos usar a pesquisa dessa página, geralmente funciona bem.