# Host Selection

A maioria das vezes não queremos rodar os comandos em todos os hosts. Por isso, podemos criar grupos e roles para dividir a aplicação de nossos comandos.

Com isso, usaremos em alguns subconjuntos.

Para fazer com todos os hosts, assim como já vimos, usamos o parâmetro "all"

```
ansible --list-hosts all
```

Podemos usar wildcards:

```
ansible --list-hosts "*"
```

## Subconjuntos

Mas se quisermos rodar em algum subconjunto:

```
ansible --list-hosts loadbalancer # Assim rodamos contra um grupo

ansible --list-hosts webserver # Assim rodamos contra um grupo

ansible --list-hosts db01 # Assim rodamos contra um host específico

ansible --list-hosts "app0*" # Assim rodamos contra um conjunto de hosts usando wildcard

ansible --list-hosts database:control # assim vemos de vários conjuntos diferentes

```

Podemos tratar os grupos como se fossem vetores:

```
ansible --list-hosts webserver[0]
```

Podemos excluir algum grupo:

```
ansible --list-hosts \!control
```

 Esses são as maiorias dos comandos de host selection usados. Na documentação oficial tem alguns outros, mas esses já tem um alcance muito bom!