# Roles

Olhando nossa stack agora:

Temos uma webapp em 3 níveis, cada nível com seu próprio playbook. Cada playbook tem diversas tasks referentes a camada da aplicação.

Mas quanto do nosso código pode ser usado para outras aplicações?

Para resolver isso, o Ansible nos oferecem as Roles. Que é uma pasta onde podemos colocar as peculiaridades de uma aplicação. E manter aquilo que é genérico na parte de fora.

Podemos ver como outras pessoas implementaram as roles com o Ansible-Galaxy, que é um repositório público para as roles.

---

## Ansible Galaxy

Em nossa máquina de controle, dentro da pasta do Ansible, crie uma pasta Roles:

```sh
mkdir roles
cd roles
```

E agora basta usar:

```
ansible-galaxy init control
```

Veja que a pasta control foi criada.

Agora, inicie para cada tier da aplicação:

```bash
ansible-galaxy init nginx
ansible-galaxy init apache2
ansible-galaxy init demo_app
ansible-galaxy init mysql
```

---

## Conversão para Roles: Tasks e Handlers

Vamos começar a migrar nossa aplicação.

Para migrar, é basicamente pegar o conteúdo de nossos playbooks e colocá-los nos conteúdos de roles.

---

### Control

roles/control/tasks/main.yml:

```yaml
---
- name: install tolls
  apt: name={{item}} state=present update_cache=yes
  with_items:
    - curl
    - python-httplib2
```

Para o templates/control.yml:

```yaml
---
- hosts: control
  become: true
  roles:
    - control
```

Agora o nosso playbook vai procurar por alguma role de control para ser executado.

Para executar:

```bash
ansible-playbook control.yml
```

O output é o mesmo de antes.

---

### Database

roles/mysql/tasks/main.yml:

```yaml
---
- name: install tools
  apt: name={{item}} state=present udpate_cache=yes
  with_items:
    - python-mysqldb

  - name: install mysql-server
    apt: name=mysql-server state=present update_cache=yes

  - name: ensure mysql listening on all ports
    lineinfile: dest=/etc/mysql/my.cnf regexp=^bind-address line="bind-address = 0.0.0.0"
  notify: restart mysql

  - name: ensure mysql started
    service: name=mysql state=started enabled=yes

  - name: create demo db
    mysql_db: name=demo state=present

  - name: create user db
    mysql_user: name=demo password=demo priv=demo.*:ALL host='% state=present
```

Mas aqui temos um problemas, pois no db.yml temos os handlers. Similar as tasks, ao invés de colocar em mysql/tasks, vamos colocar em mysql/handlers.

roles/mysql/handlers/main.yml:

```yaml
---
- name: restart mysql
  service: name=mysql state=restarted
```

templates/db.yml:

```yaml
---
- hosts: database
  become: true
  roles:
    - mysql
```

---

### Loadbalancer

Sendo um pouco diiferentes dos anteriores, esses tiers precisam que files sejam movidos, além das tasks e dos handlers

roles/nginx/tasks/main.yml:

```yaml
---
- name: install tools
  apt: name={{items}} state=present update_cache=yes
  with_items:
    - python-httplib2

- name: install nginx
  apt: name=nginx state=present update_cache=yes

- name: configure nginx site
  template: src=templates/nginx.conf.j2 dest=/etc/nginx/sites-avaliable/demo mode=0644
  notify: restart ngix

- name: de-activate default nginx site
  file: path=/etc/nginx/sites-enable/default state=absent
  notify: restart nginx

- name: activate demo nginx site
  file: src=/etc/nginx/sites-avaliable dest=/etc/nginx/sites-enabled/demo state=link
  notify: restart nginx

- name: ensure nginx started
  service: name=nginx state=started enabled=yes
```

roles/nginx/handlers/main.yml:

```yaml
---
- name: restart nginx
  service: name=nginx state=restarted
```

Mas nosso loadbalancer também tem um template:

```bash
mv templates/nginx.conf.j2 roles/nginx/templates
```

E precisamos alterar o arquivo /roles;nginx/tasks/main.yml:


```yaml
---
- name: configure nginx site
  template: src=nginx.conf.j2 dest=/etc/nginx/sites-avaliable/demo mode=0644
  notify: restart ngix

```

---

### Webserver

Vamos transformar esse tier em duas roles distintas. Uma para o Apache, outra para a Demo app.

roles/apache2/handlers/main.yml:

```yaml
---
-  name: restart apache2
   service: name=apache2 state=restarted
```

roles/apache2/tasks/main.yml:

```yaml
---
- name: install web components
  apt: name={{item}} state=present update_cache=yes
  with_items:
    - apache2
    - libapache2-mod-wsgi

- name: ensure mod_wsgi enabled
  apache2_module: state=present name=wsgi
  notify: restart apache2

- name: de-activate default apache site
  file: path=/etc/apache2/sites-enable/000-default.conf state=absent
  notify: restart apache2

- name: ensure apache2 started
  service: name=apache2 state=started enabled=yes

```

roles/demo_app/tasks/main.yml:

```yaml
---
- name: install web components
  apt: name={{item}} state=present update_cache=yes
  with_items:
    - python-pip
    - python-virtualenv
    - python-mysqldb

- name: copy demo app source
  copy: src=demo/app/ dest=/var/www/demo mode=0755
  notify: restart apache2
    
- name: copy demo config virtual host
  copy: src=demo/demo.conf dest=/etc/apache2/sites-avaliable mode=0755
  notify: restart apache2

- name: setup python virtual env.
  pip: requirements=/var/www/demo/requirements.txt virtualenv=/var/www/demo/.venv
  notify: restart apache2
        
- name: activate demo apache site
  file: src=/etc/apache2/sites-avaliable/demo.conf dest=/etc/apache2/sites-enabled/demo.conf state=link
  notify: restart apache2

```

E agora precisamos mover todo o código de demo/ para roles/demo_app/files

```bash
mv demo/ roles/demo_app/files/
```