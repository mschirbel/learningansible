# Tasks

Tasks são comandos que usamos para gerenciar outros hosts.

Como por exemplo, o comando ping:

```
ansible -m ping all
```

Veja que recebemos o status de todos nossos Hosts.

Umas task é o bloco mais simples de qualquer configuração que o Ansible faz.

## Formação

Toda task é formada de um módulo e de um comando relacionado com aquele módulo.

```
ansible -m command -a "hostname" all
```

Nesse caso, usamos o módulo de comando e o parâmetro "-a" para indicar qual comando queremos executar nos hosts.

Para alguns comandos não precisamos explicitar os módulos, mas por garantia é sempre bom fazê-lo.

```
ansible  -a "hostname" all
```

Veja que ao executarmos esse comando recebemos alguns outputs.

O primeiro é o host, o status e o código de retorno. Nesse caso é "rc=0" e por último recebemos o resultado do comando.

O Ansible está conectando via SSH nas máquinas e executando, via Python, alguns subprocess para rodar esse comando no shell. Ansible usa Paramiko, uma lib de Python para SSH.

## Sucesso ou Falha

Ao executarmos o segundo comando:

```
ansible  -a "/bin/false" all
```

Notamos que o o comando não foi bem sucedido, pois /bin/false é um comando em linux que retorna algo não 0.
Logo, o Ansible entende isso como falha, pois 0 é sucesso e qualquer coisa diferente de 0 é falha.

Mesmo que o comando tenha rodado com sucesso, o retorno foi de falha.