# Application Modules

Agora que nossos arquivos de configuração estão em lugar, assim como nosso arquivos de aplicação, precisamos instalar algumas dependências.

## Pip

Agora vamos criar uma nova task:

```yaml
---
-  hosts: webserver
   become: true
   tasks:
     -  name: install web components
        apt: name={{item}} state=present update_cache=yes
        with_items:
          - apache2
          - libapache2-mod-wsgi
          - python-pip
          - python-virtualenv

     -  name: ensure apache2 started
        service: name=apache2 state=started enabled=yes

     -  name: ensure mod_wsgi enabled
        apache2_module: state=present name=wsgi
        notify: restart apache2

     - name: copy demo app source
       copy: src=demo/app/ dest=/var/www/demo mode=0755
       notify: restart apache2
    
     - name: copy demo config virtual host
       copy: src=demo/demo.conf dest=/etc/apache2/sites-avaliable mode=0755
       notify: restart apache2

     - name: setup python virtual env.
       pip: requirements=/var/www/demo/requirements.txt virtualenv=/var/www/demo/.venv
       notify: restart apache2
        
   handlers:
     -  name: restart apache2
        service: name=apache2 state=restarted
```

O Ansible traz consigo um módulo para o pip, ele que irá instalar as dependências e configurar nosso virtual environment.

É como se fosse um container, uma pasta que será isolada as dependências no sistema.

Nesse caso, vamos usar um arquivo requirements.txt para agrupar todas as nossas dependências. E como ele está dentro de nossa app, basta referenciá-lo.

Esse arquivo contém em cada linha o nome de um módulo do pip.

E o segundo argumento que passamos é o local do virtual environment, que será onde as dependências serão instaladas.

Agora basta executar o playbook:

```sh
ansible-playbook webserver.yml
```

## MySQL - mysql_db & mysql_user

Agora precisamos fazer a conexão com o banco de dados. Para isso, precisamos de um usuário e de uma senha.

Antes de tudo, precisamos instalar a lib do Python que cuida do MySQL:

```yaml
---
-  hosts: webserver
   become: true
   tasks:
     -  name: install web components
        apt: name={{item}} state=present update_cache=yes
        with_items:
          - apache2
          - libapache2-mod-wsgi
          - python-pip
          - python-virtualenv
          - python-mysqldb

     -  name: ensure apache2 started
        service: name=apache2 state=started enabled=yes

     -  name: ensure mod_wsgi enabled
        apache2_module: state=present name=wsgi
        notify: restart apache2

     - name: copy demo app source
       copy: src=demo/app/ dest=/var/www/demo mode=0755
       notify: restart apache2
    
     - name: copy demo config virtual host
       copy: src=demo/demo.conf dest=/etc/apache2/sites-avaliable mode=0755
       notify: restart apache2

     - name: setup python virtual env.
       pip: requirements=/var/www/demo/requirements.txt virtualenv=/var/www/demo/.venv
       notify: restart apache2

     - name: de-activate default apache site
       file: path=/etc/apache2/sites-enable/000-default.conf state=absent
       notify: restart apache2
        
     - name: activate demo apache site
       file: src=/etc/apache2/sites-avaliable/demo.conf dest=/etc/apache2/sites-enabled/demo.conf state=link
       notify: restart apache2

   handlers:
     -  name: restart apache2
        service: name=apache2 state=restarted
```

Adicione essa última linha no yml do webserver. Ela é necessária para o módulo do mysql do Ansible

E execute o playbook.

---

Agora, devemos usar o módulo MySQL do Ansible para realizar as alterações:

Agora, no yml do banco:

```yaml
---
- hosts: database
  become: true
  tasks:
    - name: install tools
      apt: name={{item}} state=present udpate_cache=yes
      with_items:
       - python-mysqldb

    - name: install mysql-server
      apt: name=mysql-server state=present update_cache=yes

    - name: ensure mysql started
      service: name=mysql state=started enabled=yes

    - name: ensure mysql listening on all ports
      lineinfile: dest=/etc/mysql/my.cnf regexp=^bind-address line="bind-address = 0.0.0.0"
      notify: restart mysql

    - name: create demo db
      mysql_db: name=demo state=present

    - name: create user db
      mysql_user: name=demo password=demo priv=demo.*:ALL host='% state=present

  handlers:
    - name: restart mysql
      service: name=mysql state=restarted
  
```

Criamos uma task inicial para verificar se lib mysqldb está instalada.

Depois criamos uma nova task para o usuário do banco e qual banco usar.

A task do banco é bem simples, apenas dar um nome para o banco.

Na task de usuário, setamos o user e a sua senha. Também devemos especificar de quais hosts aceitamos a conexão com o banco, e o wildcard para o mysql é o '%'.

Agora basta executar o playbook do banco de dados.

E testamos a conexão:

```bash
curl app01/db
curl app02/db
curl lb01/db
```