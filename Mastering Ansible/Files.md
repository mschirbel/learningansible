# Files

Agora vamos tratar nossa aplicação Python.

## Copy

Até agora nosso webserver está assim:

```yaml
---
-  hosts: webserver
   become: true
   tasks:
     -  name: install web components
        apt: name={{item}} state=present update_cache=yes
        with_items:
          - apache2
          - libapache2-mod-wsgi
          - python-pip
          - python-virtualenv
     -  name: ensure apache2 started
        service: name=apache2 state=started enabled=yes
     -  name: ensure mod_wsgi enabled
        apache2_module: state=present name=wsgi
        notify: restart apache2
   handlers:
     -  name: restart apache2
        service: name=apache2 state=restarted
```

Mas precisamos configurar nosso Apache. Para isso, precisamos editar os arquivos de configuração e depois passá-los para as máquinas.

O Ansible tem um módulo que nos permite fazer isso: Copy.

O Copy nos permite passar os arquivos da máquina de controle para os hosts.

Para isso, vamos criar uma nova task:

```yaml
---
-  hosts: webserver
   become: true
   tasks:
     -  name: install web components
        apt: name={{item}} state=present update_cache=yes
        with_items:
          - apache2
          - libapache2-mod-wsgi
          - python-pip
          - python-virtualenv

     -  name: ensure apache2 started
        service: name=apache2 state=started enabled=yes

     -  name: ensure mod_wsgi enabled
        apache2_module: state=present name=wsgi
        notify: restart apache2

     - name: copy demo app source
       copy: src=demo/app/ dest=var/www/demo mode=0755
       notify: restart apache2
        
   handlers:
     -  name: restart apache2
        service: name=apache2 state=restarted
```

Na task de copy temos a source, que é local e o destino, mesmo que a pasta não exista, o Ansible cria.

E temos o mode, para as permissões dos arquivos.

E depois podemos chamar o notify, assim como já aprendemos.

Agora vamos criar uma task para as configurações:

```yaml
---
-  hosts: webserver
   become: true
   tasks:
     -  name: install web components
        apt: name={{item}} state=present update_cache=yes
        with_items:
          - apache2
          - libapache2-mod-wsgi
          - python-pip
          - python-virtualenv

     -  name: ensure apache2 started
        service: name=apache2 state=started enabled=yes

     -  name: ensure mod_wsgi enabled
        apache2_module: state=present name=wsgi
        notify: restart apache2

     - name: copy demo app source
       copy: src=demo/app/ dest=/var/www/demo mode=0755
       notify: restart apache2
    
     - name: copy demo config virtual host
       copy: src=demo/demo.conf dest=/etc/apache2/sites-avaliable mode=0755
       notify: restart apache2
        
   handlers:
     -  name: restart apache2
        service: name=apache2 state=restarted
```

Agora basta executar o playbook:

```sh
ansible-playbook webserver.yml
```

## File

Agora que temos nossos arquivos nos lugares, precisamos desativar o site default do Apache e ativar o nosso.

Para isso, vamos usar o modulo File.

Para ver o sites disponíveis do Apache:

```bash
ls -la /etc/apache2/sites-avaliable/
```

Veja que existe um arquivo 000-default.conf e o nosso demo.conf - pois copiamos ele na ultima aula.

Para vermos os sites que estão ativos:

```bash
ls -la /etc/apache2/sites-enabled/
```

Agora precisamos trocar isso com o módulo do Ansible.

```yaml
---
-  hosts: webserver
   become: true
   tasks:
     -  name: install web components
        apt: name={{item}} state=present update_cache=yes
        with_items:
          - apache2
          - libapache2-mod-wsgi
          - python-pip
          - python-virtualenv

     -  name: ensure apache2 started
        service: name=apache2 state=started enabled=yes

     -  name: ensure mod_wsgi enabled
        apache2_module: state=present name=wsgi
        notify: restart apache2

     - name: copy demo app source
       copy: src=demo/app/ dest=/var/www/demo mode=0755
       notify: restart apache2
    
     - name: copy demo config virtual host
       copy: src=demo/demo.conf dest=/etc/apache2/sites-avaliable mode=0755
       notify: restart apache2

     - name: setup python virtual env.
       pip: requirements=/var/www/demo/requirements.txt virtualenv=/var/www/demo/.venv
       notify: restart apache2

     - name: de-activate default apache site
       file: path=/etc/apache2/sites-enable/000-default.conf state=absent
       notify: restart apache2
        
     - name: activate demo apache site
       file: src=/etc/apache2/sites-avaliable/demo.conf dest=/etc/apache2/sites-enabled/demo.conf state=link
       notify: restart apache2

   handlers:
     -  name: restart apache2
        service: name=apache2 state=restarted
```

Adicionamos duas tasks.

Essa task:

```yaml
 - name: de-activate default apache site
   file: path=/etc/apache2/sites-enable/000-default.conf state=absent
   notify: restart apache2
```

É responsável por apagar o arquivo default do apache. Por isso o state=absent.

E essa:

```yaml
   - name: activate demo apache site
     file: src=/etc/apache2/sites-avaliable/demo.conf dest=/etc/apache2/sites-enabled/demo.conf state=link
     notify: restart apache2
```

Essa task serve para criar o link entre nosso site e a pasta de sites enabled.

Agora basta executar o playbook:

```sh
ansible-playbook webserver.yml
```

## Templates

Template é um pouco diferente da cópia normal.

Ao invés de copiar da máquina de controle para o end host ele primeiro altera as variáveis - para proteção - e aí copia. Isso pode ser usado para proteger aquivos que contém as senhas de bancos de dados, por exemplo.

Vamos criar uma nova pasta em nossa pasta do Ansible e um novo arquivo:

```bash
mkdir templates
cd templates
nginx.conf.j2
```

E a terminação j2 significa Jinja2 filtering. Que é o modo como as variáveis serão filtradas.

Para o arquivo:

```j2
upstram demo {
{% for server in groups.webserver %}
   server {{ server }}   
{% endfor}
}

server {
   listen 80;

   location /{
      proxy_pass http://demo;
   }
}

```

Primeiramente temos um loop de Jinja, que server para aplicar as configurações em todos os hosts dentro do group webserver de nosso Ansible.

E após isso, é uma configuração padrão do nginx.

Agora vamos para o arquivo yml do nosso loadbalancer:

```yaml
---
- hosts: loadbalancer
  become: true
  tasks:
    - name: install nginx
      apt: name=nginx state=present update_cache=yes

    - name: ensure nginx started
      service: name=nginx state=started enabled=yes

    - name: configure nginx site
      template: src=templates/nginx.conf.j2 dest=/etc/nginx/sites-avaliable/demo mode=0644
      notify: restart ngix

    - name: de-activate default nginx site
      file: path=/etc/nginx/sites-enable/default state=absent
      notify: restart nginx

    - name: activate demo nginx site
      file: src=/etc/nginx/sites-avaliable dest=/etc/nginx/sites-enabled/demo state=link
      notify: restart nginx

   handlers:
     - name: restart nginx
       service: name=nginx state=restarted
```

Agora basta executar o playbook:

```sh
ansible-playbook loadbalancer.yml
```

Agora para realizar os testes:

```bash
curl app01
curl app02
```

Devem responder com a aplicação.

```bash
curl lb01
```

Repita esse comando algumas vezes. E deverá ser feito um balance para os dois servidores de aplicação.

## Lineinfile

Além da resposta da aplicação, precisamos tratar uma resposta para o banco de dados.

Antes de tudo, verifique se a porta 3306, porta do MySQL está habilitada na máquina de banco:

```bash
ssh db01
netstat -an
```

Veja também se ele somente aceita conexões de localhost, isso não pode ficar assim, pois conectaremos de outra máquina

Também veja as configurações do MySQL:

```bash
sudo less /etc/mysql/my.cnf
```

Procure por bind-address. Por default, vem configurado para localhost. Precisamos trocar somente essa linha. Para isso, vamos utilizar um módulo do Ansible, lineinfile, que permite alterarmos somente uma linha do arquivo e manter todo o resto.

Dentro do nosso arquivo de databse.yml:

```yaml
---
- hosts: database
  become: true
  tasks:
    - name: install mysql-server
      apt: name=mysql-server state=present update_cache=yes

    - name: ensure mysql started
      service: name=mysql state=started enabled=yes

    - name: ensure mysql listening on all ports
      lineinfile: dest=/etc/mysql/my.cnf regexp=^bind-address line="bind-address = 0.0.0.0"
```

O parâmetro regexp é para expressões regulares e esse em específico vai procurar uma linha que começe(^) com bind-address e vai substituir o pelo conteúdo do parâmetro line.

Agora precisamos reiniciar o MySQL e para isso vamos criar um handler:

```yaml
---
- hosts: database
  become: true
  tasks:
    - name: install mysql-server
      apt: name=mysql-server state=present update_cache=yes

    - name: ensure mysql started
      service: name=mysql state=started enabled=yes

    - name: ensure mysql listening on all ports
      lineinfile: dest=/etc/mysql/my.cnf regexp=^bind-address line="bind-address = 0.0.0.0"
      notify: restart mysql

  handlers:
    - name: restart mysql
      service: name=mysql state=restarted
  
```

Vamos executar nosso playbook:

```bash
ansible-playbook database.yml
```