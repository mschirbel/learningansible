# Packages

Agora vamos começar a configurar nosso cluster.

Criaremos um Playbook para cada tier de nossa aplicação.

Caso queira saber mais sobre boas práticas de desenvolvimento de playbooks, ![clique aqui](http://hakunin.com/six-ansible-practices)

```
touch loadbalancer.yml
touch app.yml
touch db.yml
```

## Editando os arquivos

Dentro do arquivo loadbalancer.yml:

```yaml
---
- hosts: loadbalancer
  tasks:
    - name: install nginx
      apt: name=nginx state=present update_cache=yes

```

apt é o nome do package manager. e após o nome do package manager, temos diversos key-value pairs

name é o nome do pacote a ser instalado
state serve para verificar se está instalado e se não estiver, vai buscar a versão mais recente
update_cache é como se fosse um apt update, antes da instalação, para atualizar o package manager

---

Dentro do arquivo db.yml:

```yaml
---
- hosts: database
  tasks:
    - name: install mysql-server
      apt: name=mysql-server state=present update_cache=yes
```

## Executando os Playbooks

Para executar o primeiro:

```sh
ansible-playbook loadbalancer.yml
```

Veja que não funcionou, pois não temos permissão de root. Ainda que o usuário que o Ansible usa tenha permissão de root, é necessário mandar que o Ansible use esse privilégio ao executar essa task. Para isso, adicione o key become no arquivo:

```yaml
---
- hosts: loadbalancer
  become: true
  tasks:
    - name: install nginx
      apt: name=nginx state=present update_cache=yes

```

Com become: true, o Ansible irá executar a task com sudo. Rode novamente.

```sh
ansible-playbook loadbalancer.yml
```

É possível usar o become dentro de cada task, ao usar fora, todas serão executadas como super user.

Agora não queremos mais ver o resultado printado na tela, queremos somente saber se o playbook retornou sucesso ou falha.

Se executar o comando novamente, veja que a changed não foi alterado, pois o nginx já estava instalado.

Agora altere também o arquivo db.yml:

```yaml
---
- hosts: database
  become: true
  tasks:
    - name: install mysql-server
      apt: name=mysql-server state=present update_cache=yes
```

E execute o Playbook:

```sh
ansible-playbook db.yml
```

Também vai funcionar pois precisamos de sudo para instalar o mysql server.

## Instalando packages no webserver

Nosso webserver vai precisar de múltiplas packages. Para isso, devemos usar o with_itens

```yaml
---
-  hosts: webserver
   become: true
   tasks:
     -  name: install web components
        apt: name={{item}} state=present update_cache=yes
        with_items:
          - apache2
          - libapache2-mod-wsgi
          - python-pip
          - python-virtualenv
```

Ao invés de copiar e colar diversas tasks, podemos usar um loop.

A variável {{item}} representa qual package vai ser chamada no comando do apt. E as interações estao dentro de with_items.

E execute o Playbook:

```sh
ansible-playbook webserver.yml
```

Com isso, percebemos que o Ansible executou a task nos dois hosts do webserver ao mesmo tempo.