# Advanced Execution

Vamos ver agora como otimizar a execução de nossos playbook.

Para ver quanto tempo será gasto na execução:

```bash
time ansible-playbook site.yml
time ansible-playbook stack_status.yml
time ansible-playbook stack_restart.yml
```

## Removendo passos desnecessários

O primeiro passo de qualquer execução é sempre 'Gathering Facts'. Facts são aqueles passos que podem ser usados em alguma execução. Em nosso caso, nós usamos no MySQL, mas não usamos em nenhum outro lugar.

Podemos desativar isso:

templates/stack_status.yml:

```yaml
---
- hosts: loadbalancer
  become: true
  gather_facts: false
  tasks:
    - name: verify nginx status
      command: service nginx status

    - name: verify nginx is listening on port 80
      wait_for: port=80 timeout=5

- hosts: webserver
  become: true
  gather_facts: false
  tasks:
    - name: verify apache status
      command: service apache status
    
    - name: verify apache is listening on port 80
      wait_for: port=80 timeout=5

- hosts: control
  gather_facts: false
  tasks:
    - name: verify end-to-end response
      uri: url=http://{{item}} return_content=yes
      with_item: groups.loadbalancer
      register: lb_index
    
    - fail: msg="index failed to return content"
      when: "'Hello, from sunny' not it item.content"
      with_items: "{{lb_index.results}}
      
- hosts: loadbalancer
  gather_facts: false
  tasks:
    - name: verify backend response
      uri: url=http://{{item}} return_content=yes
      with_item: groups.webserver
      register: app_index
    
    - fail: msg="index failed to return content"
      when: "'Hello, from sunny {{item.item}}!' not it item.content"
      with_items: "{{app_index.results}}
```

O único que não pode receber isso é o DB, porque usamos os facts para realizar algumas tasks.

Agora verifique o tempo de execução:

```bash
time ansible-playbook site.yml
time ansible-playbook stack_status.yml
time ansible-playbook stack_restart.yml
```

Faça o mesmo no stack_restart.yml:

```yaml
---
# Bring stack down
- hosts: loadbalancer
  become: true
  gather_facts: false
  tasks:
    - service: name=nginx state=stopped
    - wait_for: port=80 state=drained

- hosts: webserver
  become: true
  gather_facts: false
  tasks:
    - service: name=apache2 state=stopped
    - wait_for: port=80 state=stopped

# Restart mysql
- hosts: database
  become: true
  gather_facts: false
  tasks:
    - service: name=mysql state=restarted
    - wait_for: host={{ ansible_eht0.ipv4.address }} port=3306 state=started

# Bring stack up
- hosts: webserver
  become: true
  gather_facts: false
  tasks:
    - service: name=apache2 state=started
    - wait_for: port=80 state=started

- hosts: loadbalancer
  become: true
  gather_facts: false
  tasks:
    - service: name=nginx state=started
    - wait_for: port=80
```

E veja o tempo de execução:

```bash
time ansible-playbook site.yml
time ansible-playbook stack_status.yml
time ansible-playbook stack_restart.yml
```

---

## cache_valid_time

Para o nosso arquivo site.yml percebemos que boa parte do tempo é gasto com updates ou instalando ferramentas.
Mesmo que já esteja instalado, ainda esperamos por um cache acabar.

O parâmetro *update_cache=yes* é necessário, mas somente na primeira execução. A partir da primeira, devemos pagar um tempo de execução que é totalmente irrelevante.

Para resolver isso podemos usar o *cache_valid_time* para pular o *update_cache=yes*

Em nosso arquivo templates/site.yml:

```yaml
---
- hosts: all
  become: true
  gather_facts: false
  tasks:
    - name: update apt cache
      apt: update_cache=yes cache_valid_time=86400
      
- include: control.yml
- include: db.yml
- include: webserver.yml
- include: loadbalancer.yml

```

Com isso, todos os hosts vao sofrer um *apt update* e esse cache vai ser válido por 24h(86400 segundos).

Agora podemos excluir o parâmetro *update_cache=yes* dos outros playbooks.

```bash
time ansible-playbook site.yml
time ansible-playbook stack_status.yml
time ansible-playbook stack_restart.yml
```

---

## Limit

Vamos supor que seja necessário realizar uma task em somente um dos hosts. De forma bem clara, poderíamos alterar a tag *hosts* para somente o desejado. Mas o Ansible nos permite realizar execuções em subconjuntos dos hosts que foram específicados.

```bash
ansible-playbook site.yml --limit app01
```

Veja que foi possível executar todo o nosso playbook em somente um host. E os outros grupos também foram pulados, juntamente com o app02.

---

## Tags

São valores arbitrários que podemos usar para marcar partes de nossos playbooks. Assim, podemos executar certas tags e filtrar as que não desejamos.

Por exemplo, dentro de nosso arquivo roles/control/tasks/main.yml:

```yaml
---
- name: install tolls
  apt: name={{item}} state=present
  with_items:
    - curl
    - python-httplib2
  tags: [ 'packages' ]
```

E para executar:

```bash
ansible-playbook site.yml --list-tags
```

Veja que dentro de control, foi aplicado a tag *packages*

Agora, se quiséssemos rodar somente essa tag:

```bash
ansible-playbook site.yml --tags "packages"
```

Veja que pulamos várias partes, e executamos somente a parte do control.

E para não fazer uma determinada tag:

```bash
ansible-playbook site.yml --skip-tags "packages"
```

Vamos editar também os arquivos main.yml do apache, nginx, database e demo_app e site.yml

Agora quando formos executar, podemos selecionar quais tasks não precisaremos executar, como 'packages', por exemplo.

E podem ser incluídas diversas tags em uma task.

---

## Changed_when

Uma das boas partes de executar um playbook é o PlayRecap no final que mostra o status e se houve alguma alteração nos ambientes. Especialmente ao final de um longo playbook.

Mas é bem comum existirem changes, mesmo quando sabemos que não há. Seria bom ter uma maneira de silenciar esse parâmetro.

Para isso, podemos usar o changed_when ou failed_when.

Dentro de templates/stack_status.yml:

```yaml
- hosts: loadbalancer
  become: true
  gather_facts: false
  tasks:
    - name: verify nginx status
      command: service nginx status
      changed_when: false

    - name: verify nginx is listening on port 80
      wait_for: port=80 timeout=5

- hosts: webserver
  become: true
  gather_facts: false
  tasks:
    - name: verify apache status
      command: service apache status
      changed_when: false
    
    - name: verify apache is listening on port 80
      wait_for: port=80 timeout=5

- hosts: db
  become: true
  tasks:
    - name: verify mysql status
      command: service mysql status
      changed_when: false
    
```

Agora quando executarmos, o comando vai nos dizer se foi sucesso ou não. É basicamente isso que o changed_when está falando: só indique que algo mudou quando o resultado for false.

Para o changed_when devemos passar um valor boolean que servirá de comparação.

Um exemplo mais complexo:

Dentro de roles/nginx/tasks/main.yml temos uma task para buscar os sites ativos. Dentro dessa task usamos um módulo de shell, isso nos retornará uma mudança quase sempre. Mas nesse caso não podemos passar um *false* pois o resultado não seria boolean. Para isso, podemos inserir uma função Python:

```yaml
- name: get active sites
  shell: ls -1 /etc/nginx/sites-enabled
  register: active
  changed_when: "active.stdout_lines != sites.keys()"
  tags: [ 'configure' ]
```

O que isso faz é comparar os sites que temos em default com o resultado do comando em shell.

---

## Pipeline

Atualmente o Ansible pode usar uma função para SSH chamada ControlPersiste, que permite que o client do SSH possa reusar conexões antigas. Mas isso é somente para as versões mais recentes de Ansible.

Pode ser lido mais ![aqui](https://docs.ansible.com/ansible/playbooks_acceleration.html)

Para ativar o pipelining:

```bash
sudo vi /etc/ansible/ansible.cfg
```

Dentro do arquivo, temos uma seção para SSH:

```
#pipelining = False
```

Troque dentro do nosso arquivo ansible.cfg:

```
[defaults]
inventory =./dev
vault_password_file = templates/.vault_pass.txt

[ssh_connection]
pipelining = True
```

E dentro do arquivo visudo:

```bash
sudo visudo
```

Edite a linha:

```
Defaults    !requiretty
```

Pois o TTy pode se tornar um problema para o Ansible.