# Sites

Agora movemos todos os arquivos necessários e substituimos tudo por roles.

Agora é necessário testar, para ver se nosso ambiente ainda está OK.

Agora vamos criar um playbook que contém todos os entrypoints da aplicação. Seja no LB, ou na APP ou no DB.

Vamos criar um novo arquivo:

```bash
cd templates
vi site.yml
```

Dentro do arquivo site.yml:

```yaml
---
- include: control.yml
- include: db.yml
- include: webserver.yml
- include: loadbalancer.yml
```

Include serve para incluir arquivos externos para receber calls dentro de um outro playbook. Tem vários propósitos

Agora basta executar

```bash
ansible-playbook site.yml
```

Tudo está OK, e não houveram mudanças, assim como esperado.