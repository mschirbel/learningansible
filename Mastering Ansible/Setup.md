# Topologia

3-Tier Web App

Temos uma máquina de controle, onde será instalado o ansible.

Teremos uma máquina para o LB, outra para o webserver e outra para o database.

Pode-se usar Vagrant, ou Docker ou até mesmo algum vendor Cloud.

## Requisitos

Ansible precisa de no mínimo Python 2.4 para funcionar, então, na máquina de controle, o Python deve estar instalado.

Um detalhe é que: Windows não pode ser usado como máquina de controle. Pode ser um target, mas não um controle

Todos comandos serão executados na máquina de controle.

E a máquina de controle deve ter acesso SSH para com as outras máquinas.

## Instalação Ansible

Podemos instalar de diversas formas, seja por meio de algum package repository:

```
sudo yum install ansible
```

ou

```
sudo dnf install ansible
```

ou

```
sudo apt-get update
sudo apt-get install software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt-get install ansible
```

Ou até mesmo fazendo um clone do repositório oficial:

```
git clone https://github.com/ansible/ansible.git
cd ./ansible
make rpm
sudo rpm -Uvh ./rpm-build/ansible-*.noarch.rpm
```

Outro jeito de instalar é via package manager do Python:

```
pip install ansible
```

Após escolher um destes métodos, basta verificar se temos acesso a cli do Ansible e a cli do Ansible Playbook:

```
ansible --help
ansible-playbook --help
ansible-galaxy --help
```