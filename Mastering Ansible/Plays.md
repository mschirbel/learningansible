# Plays

Os comandos que são executados como tasks são bons para troubleshooting, ou até mesmo análise de ambientes. Mas o real poder que o Ansible oferece são os Playbooks.

São um conjunto de tasks executados em ordem. Vem de uma arquivo escrito em YAML.

## Playbook.

Uma Play é um conjunto de hosts e algum comando a ser executado contra esses hosts.

YAML é uma linguagem de marcação. E é a linguagem oficial que o Ansible usa.

Para criar um Playbook:

```
touch hostname.yml
```

Dentro do Playbook, começamos sempre da mesma forma:

```yaml
---
    - hosts: all
      tasks:
        - command: hostname
```

Com um playbook podemos executar diversas taks em sequência.

Podemos adicionar mais comandos nesse playbook para realizar nossos desejos nos hosts.

E podemos executar o arquivo que está em um arquivo. Ou seja, podemos versioná-lo. E um time todo pode fazer a colaboração sobre esse arquivo.

## Executando o Playbook

Para executar o playbook:

```
ansible-playbook [path]/hostname.yml
```

Veja que existem algumas diferenças:

1. Ansible usa Ad Hoc executions, ou seja, uma de cada vez. O playbook faz todas sequencialmente
2. Veja que não passamos nenhum parâmetro e também não especificamos nenhum host. No playbook tudo fica dentro do código.
3. O output também é diferente. Temos um passo a mais: Gathering Facts, que é o Ansible reunindo informações sobre os hosts.
4. O próximo passo é executar uma task, como se fosse um AdHoc, normal.
5. E o último passo, retorno do comando, mudou também. Agora vemos que só nos importamos em saber se a execução aconteceu e se houve algum erro.
6. Vemos o número de changes que foram realizadas, se o host estava em alcance e se houve falhas.
7. Se não houve nenhuma change, quer dizer que o resultado vai ser 'OK' e aquele passo foi pulado.

Agora vamos modificar nosso playbook para dar um nome a nossa task:

```yaml
---
    - hosts: all
      tasks:
        - name: get server hostname
          command: hostname
```

Agora basta executar:

```
ansible-playbook [path]/hostname.yml
```

Vemos que temos um nome para nossa task, o que pode ser útil quando tivermos diversas tasks dentro do mesmo Playbook.